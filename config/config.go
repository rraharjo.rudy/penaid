package config

import (
	"github.com/spf13/viper"
)

type Config struct {
	APPVersion      string `mapstructure:"APP_VERSION"`
	APPDebug        string `mapstructure:"APP_DEBUG"`
	APPPort         string `mapstructure:"APP_PORT"`
	LogginStdout    bool   `mapstructure:"LOGGIN_STDOUT"`
	LogginLevel     string `mapstructure:"LOGGIN_LEVEL"`
	LogginPath      string `mapstructure:"LOGGIN_PATH"`
	LogginGinStdout bool   `mapstructure:"LOGGIN_GIN_STDOUT"`
	LogginGinLevel  string `mapstructure:"LOGGIN_GIN_LEVEL"`
	LogginGinPath   string `mapstructure:"LOGGIN_GIN_PATH"`
	DBConnection    string `mapstructure:"DB_CONNECTION"`
	DBHost          string `mapstructure:"DB_HOST"`
	DBPort          int    `mapstructure:"DB_PORT"`
	DBDatabase      string `mapstructure:"DB_DATABASE"`
	DBUsername      string `mapstructure:"DB_USERNAME"`
	DBPassword      string `mapstructure:"DB_PASSWORD"`
}

func SetupConfiguration() (config Config, err error) {

	viper.SetConfigName("app")
	viper.SetConfigType("env")
	viper.AddConfigPath("./config")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}
