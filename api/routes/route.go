package routes

import (
	"database/sql"
	"fmt"
	"penaid/config"
	"penaid/internal/user/handler"
	"penaid/pkg/logger"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func SetupRoute(envVars config.Config, db *sql.DB) {

	gin.SetMode(gin.DebugMode)
	r := gin.New()

	// gin.DebugPrintRouteFunc = func(httpMethod string, absolutePath string, handlerName string, nuHandlers int) {
	// 	log.Printf("Format Endpoint Information is %v %v %v %v\n", httpMethod, absolutePath, handlerName, nuHandlers)
	// }

	// f, _ := os.Create("logs/gin.log")
	// gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	r.Use(gin.Logger())
	// r.Use(logger.LoggerToFile(envVars), recover.Recover())
	r.Use(logger.LoggerToFile(envVars))

	// r.Use(gin.LoggerWithFormatter(logger.LoggerFormatterJson)) // Loggin with Gin show on console

	r.Static("/assets", "./assets")
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"POST, GET, PUT"},
		AllowHeaders:     []string{"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With"},
		ExposeHeaders:    []string{"*"},
		AllowCredentials: true,
	}))

	api := r.Group("/api")
	handler.CreateUserHandler(api.Group("/user"))
	err := r.Run(fmt.Sprintf(":%v", envVars.APPPort))

	if err != nil {
		return
	}

}
