package handler

import (
	"penaid/pkg/logger"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
}

func CreateUserHandler(r *gin.RouterGroup) {
	userHandler := UserHandler{}

	r.POST("/create", userHandler.Create)
	r.GET("/read", userHandler.Read)
	r.GET("/read/:id", userHandler.ReadById)
	r.PUT("/update", userHandler.Update)
	r.POST("/delete", userHandler.Delete)
	r.GET("/trash", userHandler.Trash)
	r.POST("/restore", userHandler.Restore)
}

func (r *UserHandler) Create(c *gin.Context) {
	c.JSON(200, "Create")
}

func (r *UserHandler) Read(c *gin.Context) {
	logger.LogInfo("[UserHandler]... Read User", c)
	c.JSON(200, "Read")
}

func (r *UserHandler) ReadById(c *gin.Context) {
	c.JSON(200, "Read By Id")
}

func (r *UserHandler) Update(c *gin.Context) {
	c.JSON(200, "Update")
}

func (r *UserHandler) Delete(c *gin.Context) {

	c.JSON(200, "Deleted")
}

func (r *UserHandler) Trash(c *gin.Context) {

	c.JSON(200, "Trash")
}

func (r *UserHandler) Restore(c *gin.Context) {

	c.JSON(200, "Restore")
}
