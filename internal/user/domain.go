package user

import (
	"penaid/internal/dto"
	"time"
)

type Users struct {
	UserId             int
	Name               string
	Email              string
	Password           string
	Avatar             string
	ActivationToken    string
	Status             bool
	EmailVerifiedAt    int64
	LastLoginAt        int64
	CreatedAtEpochTime int64
	UpdatedAtEpochTime int64
	CreatedAt          time.Time
	UpdatedAt          time.Time
}

type IRepository interface {
	Save(user Users) dto.RepositoryResult
	FindAll() dto.RepositoryResult
	FindById(Id int) dto.RepositoryResult
	FindByEmail(email string) dto.RepositoryResult
	FindByUsername(username string) dto.RepositoryResult
	FindByIdWithRelation(Id int) dto.RepositoryResult
	Update(user Users) dto.RepositoryResult
	SoftDelete(user Users) dto.RepositoryResult
	FindAllWithTrashed() dto.RepositoryResult
	FindSingleTrashedById(Id int) dto.RepositoryResult
	Restore(Id int) dto.RepositoryResult
}

type IService interface {
	Create(user Users) dto.Response
	Read() dto.Response
	ReadById(Id int) dto.Response
	ReadByEmail(email string) dto.Response
	ReadByUsername(username string) dto.Response
	ReadByIdWithRelation(Id int) dto.Response
	Update(user Users) dto.Response
	Delete(Id int) dto.Response
	Trash() dto.Response
	Restore(Id int) dto.Response
}
