package recover

import (
	"fmt"
	"penaid/pkg/utils"

	"github.com/gin-gonic/gin"
)

func Recover() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if r := recover(); r != nil {
				utils.Panic(fmt.Sprintf("%s", r))
			}
		}()
		c.Next()
	}
}
