package postgres

import (
	"database/sql"
	"fmt"
	"penaid/config"
	"penaid/pkg/utils"

	_ "github.com/lib/pq"
)

// type Database struct {
// 	db *sql.DB
// }

func ConnetDBPsql(envVars config.Config) (*sql.DB, error) {

	sqlDsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		envVars.DBHost,
		envVars.DBPort,
		envVars.DBUsername,
		envVars.DBPassword,
		envVars.DBDatabase)

	db, err := sql.Open("postgres", sqlDsn)
	utils.PanicError(err)

	db.SetConnMaxLifetime(0)
	db.SetMaxIdleConns(50)
	db.SetMaxOpenConns(50)

	err = db.Ping()
	utils.PanicError(err)

	return db, nil
	// return &Database{db: db}, nil

}

// func (d *Database) CloseDB() {
// 	d.db.Close()
// }

// func (d *Database) GetDB() *sql.DB {
// 	return d.db
// }
