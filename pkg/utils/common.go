package utils

import (
	"log"
	"regexp"
	"strings"
	"time"
)

func RemoveSpecialChar(value string) string {
	re, err := regexp.Compile(`[^\w]`)
	if err != nil {
		log.Fatal(err.Error())
	}
	value = re.ReplaceAllString(value, " ")
	return strings.TrimSpace(value)
}

func GetTimeStr() string {
	return time.Now().Format("2023-09-01 15:04:05")
}

// 获取当前时间戳
func GetTimeUnix() int64 {
	return time.Now().Unix()
}
