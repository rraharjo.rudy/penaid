package utils

import (
	"fmt"
	"penaid/internal/dto"
	"strings"

	"github.com/go-playground/validator/v10"
)

func ValidationInputResponse(err error) (res dto.ValidationResponse) {
	res.Success = false
	var validations []dto.Validation
	for _, e := range err.(validator.ValidationErrors) {

		field, rule := strings.ToLower(e.Field()), strings.ToLower(e.Tag())
		validation := dto.Validation{Field: field, Message: GenerateValidationMessage(field, rule)}
		validations = append(validations, validation)
	}
	res.Message = "Incorrect username or password"
	res.Data = validations
	return res
}

func ValidationErrorResponse(err error) []string {
	var errors []string

	for _, e := range err.(validator.ValidationErrors) {
		errors = append(errors, e.Error())
	}

	return errors
}

func GenerateValidationMessage(field string, rule string) (message string) {
	switch rule {
	// required rule
	case "required":
		return fmt.Sprintf("Field '%s' is '%s'.", field, rule)
	// you can add another validator.v8 rule here
	default:
		return fmt.Sprintf("Field '%s' is not valid.", field)
	}
}
