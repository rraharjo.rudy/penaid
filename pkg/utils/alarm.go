package utils

import (
	"encoding/json"
	"fmt"
	"path/filepath"
	"runtime"
	"strings"
)

type errorString struct {
	s string
}

type errorInfo struct {
	Time     string `json:"time"`
	Alarm    string `json:"alarm"`
	Message  string `json:"message"`
	Filename string `json:"filename"`
	Line     int    `json:"line"`
	Funcname string `json:"funcname"`
}

func (e *errorString) Error() string {
	return e.s
}

func New(text string) error {
	Alarm("INFO", text, 2)
	return &errorString{text}
}

func Email(text string) error {
	Alarm("EMAIL", text, 2)
	return &errorString{text}
}

func Sms(text string) error {
	Alarm("SMS", text, 2)
	return &errorString{text}
}

func WeChat(text string) error {
	Alarm("WX", text, 2)
	return &errorString{text}
}

// Panic
func Panic(text string) error {
	Alarm("PANIC", text, 5)
	return &errorString{text}
}

func Alarm(level string, str string, skip int) {

	currentTime := GetTimeStr()

	fileName, line, functionName := "?", 0, "?"

	pc, fileName, line, ok := runtime.Caller(skip)
	if ok {
		functionName = runtime.FuncForPC(pc).Name()
		functionName = filepath.Ext(functionName)
		functionName = strings.TrimPrefix(functionName, ".")
	}

	var msg = errorInfo{
		Time:     currentTime,
		Alarm:    level,
		Message:  str,
		Filename: fileName,
		Line:     line,
		Funcname: functionName,
	}

	jsons, errs := json.Marshal(msg)

	if errs != nil {
		fmt.Println("json marshal error:", errs)
	}

	errorJsonInfo := string(jsons)

	fmt.Println(errorJsonInfo)

	if level == "EMAIL" {
		// Run Send Email
		return
	} else if level == "INFO" {
		// Run Execution logging
		return
	} else if level == "PANIC" {
		// Run mode PANIC
		return
	}
}
