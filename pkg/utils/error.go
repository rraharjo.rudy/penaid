package utils

import "penaid/pkg/logger"

func PanicError(err error) {
	if err != nil {
		logger.PanicLn(err.Error())
		panic(err)
	}
}
