package logger

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"path"
	"penaid/config"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
)

type LogFormat struct {
	TimeStamp    time.Time
	StatusCode   int
	Latency      time.Duration
	ClientIP     string
	UserAgent    string
	Method       string
	Path         string
	ErrorMessage string
	RequestProto string
}

type bodyLogWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

type EnvVariable struct {
	EnvV config.Config
}

func LoggerInit(envVars config.Config) {

	env := &EnvVariable{envVars}
	customFormatter := new(logrus.JSONFormatter)
	customFormatter.PrettyPrint = true
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	logrus.SetFormatter(customFormatter)
	logrus.SetReportCaller(true)
	logLevel := env.EnvV.LogginLevel
	setLogLevel(logLevel)

	if env.EnvV.LogginStdout {
		logrus.New().Out = os.Stdout
	} else {
		file, err := os.OpenFile("logs/"+env.EnvV.LogginPath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
		if err == nil {
			logrus.SetOutput(file)
		} else {
			fmt.Println("Failed to log.log to file init ", err.Error())
		}
	}
}

func LoggerToFile(envVars config.Config) gin.HandlerFunc {
	env := &EnvVariable{envVars}

	logFilePath := "logs/"
	logFileName := env.EnvV.LogginGinPath

	fileName := path.Join(logFilePath, logFileName)

	// fmt.Println(fileName)

	file, err := os.OpenFile(fileName, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println("err", err)
	}

	//instantiation
	logger := logrus.New()
	//Set output
	logger.Out = file
	//Set log level
	setLogLevel(env.EnvV.LogginLevel)

	logWriter, _ := rotatelogs.New(

		fileName+".%Y%m%d.log",

		rotatelogs.WithLinkName(fileName),

		rotatelogs.WithMaxAge(7*24*time.Hour),

		rotatelogs.WithRotationTime(24*time.Hour),
	)

	writeMap := lfshook.WriterMap{
		logrus.InfoLevel:  logWriter,
		logrus.FatalLevel: logWriter,
		logrus.DebugLevel: logWriter,
		logrus.WarnLevel:  logWriter,
		logrus.ErrorLevel: logWriter,
		logrus.PanicLevel: logWriter,
	}

	lfHook := lfshook.NewHook(writeMap, &logrus.JSONFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
	})

	logger.AddHook(lfHook)

	// logger.SetFormatter(&logrus.JSONFormatter{})
	// logger.SetFormatter(&logrus.TextFormatter{
	// TimestampFormat: "2006-01-02 15:04:05",
	// PrettyPrint:     true,
	// })

	return func(c *gin.Context) {

		bodyLogWriter := &bodyLogWriter{body: bytes.NewBufferString(""), ResponseWriter: c.Writer}
		c.Writer = bodyLogWriter

		startTime := time.Now()

		c.Next()

		responseBody := bodyLogWriter.body.String()

		endTime := time.Now()

		if c.Request.Method == "POST" {
			c.Request.ParseForm()
		}

		logger.WithFields(logrus.Fields{
			"request_method":    c.Request.Method,
			"request_uri":       c.Request.RequestURI,
			"request_proto":     c.Request.Proto,
			"request_useragent": c.Request.UserAgent(),
			"request_referer":   c.Request.Referer(),
			"request_post_data": c.Request.PostForm.Encode(),
			"request_client_ip": c.ClientIP(),

			"response_status_code": c.Writer.Status(),
			"response_body":        responseBody,

			"cost_time": endTime.Sub(startTime),
		}).Info()

		// // start time
		// startTime := time.Now()
		// // Processing request
		// c.Next()
		// // End time
		// endTime := time.Now()
		// // execution time
		// latencyTime := endTime.Sub(startTime)
		// // Request mode
		// reqMethod := c.Request.Method

		// _ = c.Request.UserAgent()
		// // Log format
		// logger.Infof(" 'Status code ' %3d | 'latencyTime ' %13v | 'Request IP ' %15s | 'reqMethod ' %s | 'Request User Agent ' %s | 'Request routing ' %s",
		// 	c.Writer.Status(), // Status code
		// 	latencyTime,
		// 	c.ClientIP(), // Request IP
		// 	reqMethod,
		// 	c.Request.UserAgent(), // Request User Agent
		// 	c.Request.RequestURI,  // Request routing
		// )
	}
}

func setLogLevel(logLevel string) {
	switch strings.ToLower(logLevel) {
	case "debug":
		logrus.SetLevel(logrus.DebugLevel)
	case "info":
		logrus.SetLevel(logrus.InfoLevel)
	case "warn":
		logrus.SetLevel(logrus.WarnLevel)
	case "error":
		logrus.SetLevel(logrus.ErrorLevel)
	default:
		logrus.SetLevel(logrus.DebugLevel)
	}
}

// LogInfo ...
func LogInfo(message string, c *gin.Context) {
	logrus.WithFields(logrus.Fields{
		"path":         c.Request.RequestURI,
		"x-request-id": c.Request.Header.Get("x-request-id"),
		"version":      c.Request.Header.Get("version"),
	}).Info(message)
}

// LogError ...
func (e *EnvVariable) LogError(message string, err error, c *gin.Context) {
	logrus.WithFields(logrus.Fields{
		"path":         c.Request.RequestURI,
		"error":        err.Error(),
		"x-request-id": c.Request.Header.Get("x-request-id"),
		"version":      e.EnvV.APPVersion,
	}).Error(message)
}

// LogFatal ...
func (e *EnvVariable) LogFatal(message string, errors error, c *gin.Context) {
	logrus.WithFields(logrus.Fields{
		"path":         c.Request.RequestURI,
		"error":        errors.Error(),
		"x-request-id": c.Request.Header.Get("x-request-id"),
		"version":      e.EnvV.APPVersion,
	}).Fatal(message)
}

// LogDebug ..
func (e *EnvVariable) LogDebug(message string, path string, xRequestID string, errors error) {
	logrus.WithFields(logrus.Fields{
		"path":         path,
		"x-request-id": xRequestID,
		"version":      e.EnvV.APPVersion,
		"error":        "N/A",
	}).Debug(message)
}

// Panic will exit with status code 2
func PanicLn(message string) {
	logrus.Panicln(message)
}

// Fatal will exit with status code 1
func FatalLn(message string) {
	logrus.Fatalln(message)
}

// Just log the message as info
func InfoLn(message string) {
	logrus.Infoln(message)
}

// Just log the message as warn
func WarnLn(message string) {
	logrus.Warnln(message)
}

// Just log the message as debug
func DebugLn(message string) {
	logrus.Debugln(message)
}

// Just log the message as debug
func PrintLn(message string) {
	logrus.Print(message)
}

func LoggerFormatterJson(param gin.LogFormatterParams) string {
	params := &LogFormat{
		ClientIP:     param.ClientIP,
		UserAgent:    param.Request.UserAgent(),
		TimeStamp:    param.TimeStamp,
		Method:       param.Method,
		Path:         param.Path,
		RequestProto: param.Request.Proto,
		StatusCode:   param.StatusCode,
		Latency:      param.Latency,
		ErrorMessage: param.ErrorMessage,
	}

	j, _ := json.Marshal(params)
	fmt.Print(string(j))
	return string(j)
}

func LoggerFormatter(params gin.LogFormatterParams) string {
	return fmt.Sprintf("{ %s - [%s] \"%s %s %s %d %s \"%s\" %s\"} \n",
		params.ClientIP,
		params.TimeStamp.Format(time.RFC1123),
		params.Method,
		params.Path,
		params.Request.Proto,
		params.StatusCode,
		params.Latency,
		params.Request.UserAgent(),
		params.ErrorMessage,
	)
}
