package main

import (
	"fmt"
	_routes "penaid/api/routes"
	"penaid/config"
	"penaid/pkg/db/postgres"
	"penaid/pkg/logger"
	"penaid/pkg/utils"
)

func main() {

	fmt.Println("Starting api server")

	env, err := config.SetupConfiguration()
	utils.PanicError(err)

	logger.LoggerInit(env)
	logger.InfoLn("Logger Initialized successfully")

	dbPgSqlConn, err := postgres.ConnetDBPsql(env)
	if err != nil {
		utils.PanicError(err)
	}

	defer dbPgSqlConn.Close()

	_routes.SetupRoute(env, dbPgSqlConn)

}
